grant select, insert, update on nbrjobs to MUWS_GEN_RL;
grant select on nbrbjob to MUWS_GEN_RL;
grant select on ptrecls to MUWS_GEN_RL;
grant select on IA_REF_ORGANIZATION_LEVEL_5 to MUWS_GEN_RL;
grant select on IA_REF_STANDARDIZE_DEPARTMENT to MUWS_GEN_RL;
grant select on pprccmt to MUWS_GEN_RL;
grant select on ptrrank to MUWS_GEN_RL;
grant select on perrank to MUWS_GEN_RL;
grant execute on fz_get_faculty_rank_desc to MUWS_GEN_RL;
grant execute on FZ_GET_FACULTY_RANK_CODE to MUWS_GEN_RL;
grant execute on fz_get_standardized_dept to MUWS_GEN_RL;
grant execute on f_orgn_hier2_fnc to MUWS_GEN_RL;
grant select on perappt to MUWS_GEN_RL;
grant select on ptrtenr to MUWS_GEN_RL;

grant select on nbrbjob_add to MUWS_GEN_RL;

grant select on IA_REF_STANDARDIZE_DIVISION to MUWS_GEN_RL;
grant select on IA_REF_STANDARDIZE_DEPARTMENT to MUWS_GEN_RL;
grant select on IA_REF_ORGANIZATION_LEVEL_5 to MUWS_GEN_RL;


grant execute on FZ_GET_LIBRARIAN_RANK_CODE to MUWS_GEN_RL;
grant execute on FZ_GET_LIBRARIAN_RANK_DESC to MUWS_GEN_RL;

